//
//  ViewController.swift
//  Maps
//
//  Created by Ibraheem Hindi on 7/28/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class NewLocationVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var map_view: MKMapView!
    @IBOutlet weak var save_button: UIBarButtonItem!
    @IBOutlet weak var title_field: UITextField!
    @IBOutlet weak var comment_field: UITextField!
    
    var location_manager = CLLocationManager()
    var picked_location = LocationEntry()
    var has_coordinates = false
    
    
    func showSuccessAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default) {UIAlertAction in
            self.dismiss(animated: true, completion: nil)
        })
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func centerMap(latitude: Double, longitude: Double, zoom: Double){
        let span = MKCoordinateSpanMake(zoom, zoom)
        let center_location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let region = MKCoordinateRegion(center: center_location, span: span)
        
        map_view.setRegion(region, animated: true)
    }
    
    func addPin(latitude: Double, longitude: Double, title: String, subtitle: String){
        let pin = MKPointAnnotation()
        pin.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        pin.title = title
        pin.subtitle = subtitle

        map_view.addAnnotation(pin)
    }
    
    func attachLocationSelector(){
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(onLongPress(recognizer: )))
        
        recognizer.minimumPressDuration = 1.5
        map_view.addGestureRecognizer(recognizer)
    }
    
    @objc func onLongPress(recognizer: UILongPressGestureRecognizer){
        if recognizer.state == .began{
            removeAllPins()
            let touched_point = recognizer.location(in: map_view)
            let coordinates = map_view.convert(touched_point, toCoordinateFrom: map_view)
            
            picked_location.latitude = coordinates.latitude
            picked_location.longitude = coordinates.longitude
            has_coordinates = true
            
            if picked_location.title != "" && picked_location.comment != ""{
                addPin(
                    latitude: coordinates.latitude,
                    longitude: coordinates.longitude,
                    title: picked_location.title,
                    subtitle: picked_location.comment
                )
                
                save_button.isEnabled = true
            }
        }
    }
    
    func requestPermission(){
        let alert = UIAlertController(
            title: "Error",
            message: "Please grant location access",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) {UIAlertAction in
            let settings_url = "App-Prefs:root=Privacy&path=LOCATION"
            UIApplication.shared.open(URL(string: settings_url)!, options: [:], completionHandler: nil)
        })

        self.present(alert, animated: true, completion: nil)
    }

    func locateUser(){
        location_manager.delegate = self
        location_manager.desiredAccuracy = kCLLocationAccuracyBest
        
        if CLLocationManager.locationServicesEnabled() {
            let status = CLLocationManager.authorizationStatus()

            if status == .denied || status == .restricted{
                self.requestPermission()
            }
            else if status == .notDetermined{
                location_manager.requestWhenInUseAuthorization()
            }
            else{
                map_view.showsUserLocation = true
                location_manager.startUpdatingLocation()
            }
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Please enable location services", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let user_location = locations[0] as CLLocation
        location_manager.stopUpdatingLocation()
        
        let lat = user_location.coordinate.latitude
        let long = user_location.coordinate.longitude
        
        centerMap(latitude: lat, longitude: long, zoom: 0.05)
    }
    
    func removeAllPins(){
        let pins = map_view.annotations
        for pin in pins{
            map_view.removeAnnotation(pin)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locateUser()
        attachLocationSelector()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationWillEnterForeground),
            name: .UIApplicationWillEnterForeground,
            object: nil
        )
    }
    
    @objc func applicationWillEnterForeground(){
        locateUser()
    }
    
    @IBAction func save(_ sender: Any) {
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        let context = app_delegate.persistentContainer.viewContext
        
        let entry = NSEntityDescription.insertNewObject(forEntityName: "Location", into: context)
        entry.setValue(picked_location.title, forKey: "title")
        entry.setValue(picked_location.comment, forKey: "comment")
        entry.setValue(picked_location.latitude, forKey: "latitude")
        entry.setValue(picked_location.longitude, forKey: "longitude")
        
        do {
            try context.save()
            showSuccessAlert(title: "Success", message: "Location saved successfully")
        }
        catch {
            print("Error while saving")
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        removeAllPins()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTitleChanged(_ sender: Any) {
        if title_field.text != ""{
            picked_location.title = title_field.text!
            
            if picked_location.comment != "" && has_coordinates{
                save_button.isEnabled = true
            }
        }
    }
    
    @IBAction func onTitleDone(_ sender: Any) {
        resignFirstResponder()
    }
    
    @IBAction func onCommentDone(_ sender: Any) {
        resignFirstResponder()
    }
    
    @IBAction func onCommentChanged(_ sender: Any) {
        if comment_field.text != ""{
            picked_location.comment = comment_field.text!
            
            if picked_location.title != "" && has_coordinates{
                save_button.isEnabled = true
            }
        }
    }
    
    @IBAction func standard(_ sender: Any) {
        map_view.mapType = .standard
    }
    
    @IBAction func satellite(_ sender: Any) {
        map_view.mapType = .satellite
    }
    
    @IBAction func hybrid(_ sender: Any) {
        map_view.mapType = .hybrid
    }
    
}

