//
//  Location.swift
//  Maps
//
//  Created by Ibraheem Hindi on 8/14/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import Foundation

class LocationEntry{
    var title = ""
    var comment = ""
    var latitude = 0.0
    var longitude = 0.0
}
