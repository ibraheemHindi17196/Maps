//
//  LocationVC.swift
//  Maps
//
//  Created by Ibraheem Hindi on 8/14/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import MapKit

class LocationVC: UIViewController, MKMapViewDelegate{

    @IBOutlet weak var title_label: UILabel!
    @IBOutlet weak var comment_label: UILabel!
    @IBOutlet weak var map_view: MKMapView!
    
    var location_entry = LocationEntry()
    
    func centerMap(){
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let center_location = CLLocationCoordinate2D(
            latitude: location_entry.latitude,
            longitude: location_entry.longitude
        )
        let region = MKCoordinateRegion(center: center_location, span: span)
        map_view.setRegion(region, animated: true)
    }
    
    func addPin(){
        let pin = MKPointAnnotation()
        pin.coordinate = CLLocationCoordinate2DMake(location_entry.latitude, location_entry.longitude)
        pin.title = location_entry.title
        pin.subtitle = location_entry.comment
        map_view.addAnnotation(pin)
    }
    
    func goToLocation(){
        let latitude = location_entry.latitude
        let longitude = location_entry.longitude
        let dir_url = URL(string: "http://maps.apple.com/maps?daddr=\(latitude),\(longitude)")!
        UIApplication.shared.open(dir_url, options: [:], completionHandler: nil)
    }
    
    // Add button to map pin
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let reuse_id = "myAnnotation"
        var pin_view = mapView.dequeueReusableAnnotationView(withIdentifier: reuse_id) as? MKPinAnnotationView
        
        if pin_view == nil {
            pin_view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuse_id)
            pin_view?.canShowCallout = true
            
            let button = UIButton(type: .detailDisclosure)
            pin_view?.rightCalloutAccessoryView = button
        }
        else {
            pin_view?.annotation = annotation
        }
        
        return pin_view
    }
    
    // On clicking that button
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        goToLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title_label.text = location_entry.title
        comment_label.text = location_entry.comment
        
        // Setup map
        map_view.delegate = self
        centerMap()
        addPin()
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func directions(_ sender: Any) {
        goToLocation()
    }

}








