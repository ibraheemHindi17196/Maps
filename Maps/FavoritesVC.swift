//
//  FavoritesViewController.swift
//  Maps
//
//  Created by Ibraheem Hindi on 8/13/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit
import CoreData

class FavoritesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_view: UITableView!
    
    var favorites = [LocationEntry]()
    var selected_loction = LocationEntry()
    
    func loadFavorites(){
        favorites.removeAll()
        let app_delegate = UIApplication.shared.delegate as! AppDelegate
        let context = app_delegate.persistentContainer.viewContext
        
        let fetch_request = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
        fetch_request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(fetch_request)
            var fetched_location = LocationEntry()
            
            for result in results as! [NSManagedObject] {
                if let title = result.value(forKey: "title") as? String {
                    fetched_location.title = title
                }
                
                if let comment = result.value(forKey: "comment") as? String {
                    fetched_location.comment = comment
                }
                
                if let latitude = result.value(forKey: "latitude") as? Double {
                    fetched_location.latitude = latitude
                }
                
                if let longitude = result.value(forKey: "longitude") as? Double {
                    fetched_location.longitude = longitude
                }
                
                favorites.append(fetched_location)
                fetched_location = LocationEntry()
            }
        }
        catch {
            print("Error")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadFavorites()
        
        if favorites.count == 0{
            table_view.isHidden = true
        }
        else{
            table_view.isHidden = false
            table_view.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table_view.dataSource = self
        table_view.delegate = self
        table_view.tableFooterView = UIView()
        table_view.separatorStyle = .singleLine
        table_view.separatorColor = .white
        table_view.register(UINib(nibName: "LocationView", bundle: nil), forCellReuseIdentifier: "LocationView")
    }
    
    // Specify number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favorites.count
    }
    
    // Specify custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationView", for: indexPath) as! LocationView
        
        cell.location_entry = favorites[indexPath.row]
        cell.backgroundColor = .lightGray
        
        // Put navigation button in circle
        let nav_image = cell.navigation_button.imageView!
        nav_image.layer.cornerRadius = nav_image.frame.size.width / 2
        nav_image.clipsToBounds = true
        
        return cell
    }
    
    // On selecting row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected_loction = favorites[indexPath.row]
        performSegue(withIdentifier: "toLocation", sender: self)
    }
    
    // Optionally set the cell height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 140
    }
    
    // Delete row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let target_location = favorites[indexPath.row]
            let app_delegate = UIApplication.shared.delegate as! AppDelegate
            let context = app_delegate.persistentContainer.viewContext
            
            let fetch_request = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
            fetch_request.returnsObjectsAsFaults = false
            
            do {
                let results = try context.fetch(fetch_request)
                
                for result in results as! [NSManagedObject]{
                    if let title = result.value(forKey: "title") as? String{
                        if let comment = result.value(forKey: "comment") as? String{
                            if let latitude = result.value(forKey: "latitude") as? Double{
                                if let longitude = result.value(forKey: "longitude") as? Double{
                                    
                                    if title == target_location.title &&
                                        comment == target_location.comment &&
                                        latitude == target_location.latitude &&
                                        longitude == target_location.longitude{
                                        
                                        context.delete(result)
                                        try context.save()
                                        favorites.remove(at: indexPath.row)
                                        
                                        if favorites.count == 0{
                                            table_view.isHidden = true
                                        }
                                        else{
                                            table_view.isHidden = false
                                            table_view.reloadData()
                                        }
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch {
                print("Error")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalTransitionStyle = .flipHorizontal

        if segue.identifier == "toLocation"{
            let destination = segue.destination as! LocationVC
            destination.location_entry = selected_loction
        }
    }
    
    @IBAction func addLocation(_ sender: Any) {
        performSegue(withIdentifier: "toNewLocation", sender: self)
    }
    
}
