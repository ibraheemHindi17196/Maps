//
//  LocationView.swift
//  Maps
//
//  Created by Ibraheem Hindi on 8/14/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class LocationView: UITableViewCell {
    
    @IBOutlet weak var location_title: UILabel!
    @IBOutlet weak var location_comment: UILabel!
    @IBOutlet weak var navigation_button: UIButton!
    
    var location_entry = LocationEntry()

    override func layoutSubviews() {
        location_title.text = location_entry.title
        location_comment.text = location_entry.comment
    }
    
    @IBAction func goToLocation(_ sender: Any) {
        let latitude = location_entry.latitude
        let longitude = location_entry.longitude
        let dir_url = URL(string: "http://maps.apple.com/maps?daddr=\(latitude),\(longitude)")!
        UIApplication.shared.open(dir_url, options: [:], completionHandler: nil)
    }
    
}
