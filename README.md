# Maps

An application that lets users save their favorite locations. Users can see their location on the map and move around to select any place they want and add a pin to it. They also choose a title and a comment on the selected location. The selected coordinates, title and comment get saved locally so that users can view them later on. The app handles all issues that may occur during accessing the user's location such as revoking the location permission or disabling the location services.

Demo Video : https://www.dropbox.com/s/1e2o77oksoqptg2/4-Maps.mp4?dl=0